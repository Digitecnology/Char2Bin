// created on 25/05/2006 at 03:36 a
using System;

public class MainClass
{
	public MainClass(string[] Words)
	{
		string temp = string.Empty;
		bool[] Btemp;
		
		for(int i = 0; i <= Words.Length - 1; i++)
		{
			if(i == Words.Length - 1)
			{
				temp += Words[i];
			}
			else
			{
				temp += Words[i] + " ";
			}
		}
		Console.WriteLine("--------------VALUE--------------");
		for(int i = 0; i <= temp.Length - 1; i++)
		{
			Btemp = this.ByteToBinary(Convert.ToByte(Convert.ToChar(temp.Substring(i,1))));
			for(int a = 0; a <= Btemp.Length - 1; a++)
			{
				if(Btemp[a] == true)
				{
					Console.Write("1");
				}
				else
				{
					Console.Write("0");
				}
			}
			Console.Write(" ");
		}
		Console.WriteLine();
		Console.WriteLine("---------------------------------");
	}
	
	public bool[] ByteToBinary(byte Number)
	{
		bool[] Btemp = new bool[8];
		int[] values = new int[]{128,64,32,16,8,4,2,1};
		int Itemp = Convert.ToInt32(Number);
		
		for(int i = 0; i <= Btemp.Length - 1; i++)
		{
			if(System.Math.Sign(Itemp - values[i]) == -1)
			{
				Btemp[i] = false;
			}
			else
			{
				Itemp -= values[i];
				Btemp[i] = true;
			}
		}
		
		return Btemp;
	}
	
	static void Main(string[] args)
	{
		new MainClass(args);
	}
}